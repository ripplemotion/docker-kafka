docker-kafka
===

Ce dépôt permet de construire et publier les dernières images Docker de Kafka de manière automatique.

Les versions patches des trois dernières mineures de Kafka sont publiées à chaque nouvelle version de Kafka.

Les images sont publiées dans le dépôt `ripplemotion/kafka` de DockerHub, le tag étant la version de Kafka (`x.y.z`).

Le processus est censé être autonome:

- Le pipeline `update_versions` est exécuté périodiquement par la CI bitbucket
  - Si une nouvelle version de Kafka est trouvée, alors commit avec:
    - màj fichier de versions
    - nettoyage du dossier de releases
    - génération d'un dossier pour construire chaque nouvelle image dans le dossier de releases
    - màj de la CI pour construire et publier les nouvelles images

- Un commit déclenche l'exécution de la CI qui construit et publie les images Kafka.

Kafka in Docker
===

This repository provides everything you need to run Kafka in Docker.

For convenience also contains a packaged proxy that can be used to get data from
a legacy Kafka 7 cluster into a dockerized Kafka 8.

Why?
---
The main hurdle of running Kafka in Docker is that it depends on Zookeeper.
Compared to other Kafka docker images, this one runs both Zookeeper and Kafka
in the same container. This means:

* No dependency on an external Zookeeper host, or linking to another container
* Zookeeper and Kafka are configured to work together out of the box

Run
---

```bash
docker run -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=`docker-machine ip \`docker-machine active\`` --env ADVERTISED_PORT=9092 ripplemotion/kafka
```

```bash
export KAFKA=`docker-machine ip \`docker-machine active\``:9092
kafka-console-producer.sh --broker-list $KAFKA --topic test
```

```bash
export ZOOKEEPER=`docker-machine ip \`docker-machine active\``:2181
kafka-console-consumer.sh --zookeeper $ZOOKEEPER --topic test
```

Running the proxy
-----------------

Take the same parameters as the ripplemotion/kafka image with some new ones:
 * `CONSUMER_THREADS` - the number of threads to consume the source kafka 7 with
 * `TOPICS` - whitelist of topics to mirror
 * `ZK_CONNECT` - the zookeeper connect string of the source kafka 7
 * `GROUP_ID` - the group.id to use when consuming from kafka 7

```bash
docker run -p 2181:2181 -p 9092:9092 \
    --env ADVERTISED_HOST=`boot2docker ip` \
    --env ADVERTISED_PORT=9092 \
    --env CONSUMER_THREADS=1 \
    --env TOPICS=my-topic,some-other-topic \
    --env ZK_CONNECT=kafka7zookeeper:2181/root/path \
    --env GROUP_ID=mymirror \
    ripplemotion/kafkaproxy
```

In the box
---
* **ripplemotion/kafka**

  The docker image with both Kafka and Zookeeper. Built from the `kafka`
  directory.

* **ripplemotion/kafkaproxy**

  The docker image with Kafka, Zookeeper and a Kafka 7 proxy that can be
  configured with a set of topics to mirror.

Public Builds
---

https://registry.hub.docker.com/u/ripplemotion/kafka/

https://registry.hub.docker.com/u/ripplemotion/kafkaproxy/

Build from Source
---

    docker build -t ripplemotion/kafka kafka/
    docker build -t ripplemotion/kafkaproxy kafkaproxy/

Todo
---

* Not particularily optimzed for startup time.
* Better docs

