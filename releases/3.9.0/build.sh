#!/bin/bash

set -e

IMAGE=ripplemotion/kafka
TAG_NAME=`basename $(pwd)`

docker buildx build \
    --pull --push \
    --tag $IMAGE:$TAG_NAME \
    --platform linux/arm64,linux/amd64 \
    .
