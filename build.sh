#!/bin/bash

set -e

for version in $(ls releases/)
do
   pushd "releases/$version"
   [ -x build.sh ] && ./build.sh > /dev/null
   popd
done
