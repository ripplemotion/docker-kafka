import os
import shutil
from pathlib import Path

import jinja2
from cookiecutter.main import cookiecutter
from yaml import Loader, load


def make_releases() -> None:
    with open("versions.yaml", "rt") as f:
        data = load(f, Loader=Loader)

    kafka_versions = list(data["kafka_versions"].keys())
    kafka_versions.sort()

    dirname = Path(__file__).parent
    releases_dir = dirname / "releases"
    if os.path.isdir(releases_dir):
        shutil.rmtree(releases_dir)

    for minor in kafka_versions[-3:]:  # Build latest three minors images only
        kafka_patch_version = data["kafka_versions"][minor]
        cookiecutter(
            "template",
            no_input=True,
            output_dir=str(releases_dir),
            overwrite_if_exists=True,
            extra_context={"version": kafka_patch_version},
        )


def make_bbp() -> None:
    base_dir = Path(__file__).parent / "releases"

    releases = [x.name for x in base_dir.iterdir() if x.is_dir()]
    releases.sort()
    jinja = jinja2.Environment(loader=jinja2.FileSystemLoader(Path(__file__).parent))
    template = jinja.get_template("bitbucket-pipelines.yml.j2")
    with open("bitbucket-pipelines.yml", "wt") as f:
        print(template.render(releases=releases), file=f)


if __name__ == "__main__":
    make_releases()
    make_bbp()
