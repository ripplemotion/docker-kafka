import itertools

import requests
import yaml
from packaging.version import Version

URL = "https://versions.ripplemotion.fr/kafka/1.json"


def main() -> None:
    resp = requests.get(URL)
    resp.raise_for_status()

    all_releases = [Version(v["version"]) for v in resp.json()["releases"]]

    def key(x) -> tuple[str, str]:
        return (x.major, x.minor)

    by_version = {
        k: max(g) for k, g in itertools.groupby(sorted(all_releases), key=key)
    }

    kafka_versions = {}
    for version, release in by_version.items():
        major, minor = version
        kafka_versions[f"{major}.{minor}"] = str(release)

    with open("versions.yaml", "w") as f:
        variables = {"kafka_versions": kafka_versions}
        yaml.dump(variables, f)


if __name__ == "__main__":
    main()
